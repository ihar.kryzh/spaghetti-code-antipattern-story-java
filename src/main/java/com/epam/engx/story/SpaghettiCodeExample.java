package com.epam.engx.story;

public class SpaghettiCodeExample {

  public static void main(String[] args) {
    int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    printSummedEvenNumbers(array);
  }

  /** Gets sum of even numbers and print them */
  public static void printSummedEvenNumbers(int[] array) {
    int sum = 0;
    for (int i = 0; i < array.length; i++) {
      if (isEven(array[i])) {
        sum += array[i];
      }
      if (sum > 10) {
        printExcessSumDetails(i, sum, array);
        return;
      }
    }
  }

  /** Checks if a number is even */
  public static boolean isEven(int num) {
    return num % 2 == 0;
  }

  /** Prints the excess sum and remaining elements details */
  public static void printExcessSumDetails(int index, int sum, int[] array) {
    System.out.println("Sum exceeds 10 at index " + index);
    printRemainingElements(index, array);
  }

  /** Prints remaining elements in the array from a given index */
  public static void printRemainingElements(int startIndex, int[] array) {
    for (int i = startIndex; i < array.length; i++) {
      System.out.println("Remaining elements " + array[i]);
    }
  }
}